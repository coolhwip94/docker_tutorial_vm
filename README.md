# docker_tutorial_vm
Files required to setup vm with ubuntu and docker.

### Notes
Shared folder is shared between local machine and vagrant vm.
* docker-tutorial.box is a ubuntu image with only docker installed.
* resources/setup_vagrant.docx is a writeup on how to create your own .box. (docker-turtorial.box is provided already.)
* resources/links_list.txt is just a copy and paste of links that are helpful to reference.

## Setup
* Create shared folder before running vagrant up.
* Vagrant up
* vagrant ssh
